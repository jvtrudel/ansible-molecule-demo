#! /bin/bash
#
# Préparation de l'emvironement 

VerifyPython3(){
	python3 --help >> /dev/null
	if [ $? -ne 0 ]; then
		echo "python is not available"
	        fail=1
	fi
}
VerifyPip3(){
	pip3 --help >> /dev/null
	if [ $? -ne 0 ]; then
		echo "pip3 is not available"
	        fail=1
	fi
}

VerifyDocker(){
	docker --help >> /dev/null
	if [ $? -ne 0 ]; then
		echo "docker is not available"
		fail=1
	fi
}

VerifyVirtualenv(){
	virtualenv --help >> /dev/null
	if [ $? -ne 0 ]; then
		echo "virtualenv is not available"
		fail=1
	fi
}

fail=0
VerifyPython3
VerifyPip3
VerifyDocker
VerifyVirtualenv

if [ $fail -ne 0 ]; then
	echo "One or more dependencies are not present."
	exit 1
fi

if [ -d ".venv" ]; then
	echo "A virtual environment already exists. Do you want to erase it? [y/N]"
	read erase
	if [ $erase == "y" ]; then
		rm -rf .venv
		virtualenv -p python3 .venv
	fi
else
	virtualenv -p python3 .venv
fi

source .venv/bin/activate

pip3 install -r requirements.txt

