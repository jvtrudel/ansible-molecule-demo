# Démonstration des test de playbook ansible avec molécule

Un playbook ansible doit 

  - effectuer la tâche pour laquelle il a été conçu,
  - être idempotent
  - fonctionner dans tout les cas ou échouer le plus rapidement possible (principe fail-fast)

## Quickstart

    source bootstrap.sh  # check dependencies and setup initial environment
 
## Dependencies

  Run `./bootstrap.sh` to check dependencies and setup your local environment.

  - python3 + pip3
  - virtualenv
  - docker

## Références

  - [iHow To Test Ansible Roles with Molecule on Ubuntu 16.04. Tutoriel DigitalOcean par Varun Chopra](https://www.digitalocean.com/community/tutorials/how-to-test-ansible-roles-with-molecule-on-ubuntu-16-04)
  - [Code source du projet molecule](https://github.com/ansible/molecule)
  - [Testing your Ansible roles with Molecule. Article de blogue par Jeff Geerling](https://www.jeffgeerling.com/blog/2018/testing-your-ansible-roles-molecule)
  - [Testing Strategies. Ansible documentation.](https://docs.ansible.com/ansible/latest/reference_appendices/test_strategies.html)
  - [Ansible TDD Development Using Molecule. Blogue post par Shone Jacob](https://dzone.com/articles/ansible-tdd-development-using-molecule)
